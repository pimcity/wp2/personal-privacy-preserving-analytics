Personal Privacy Preserving Analytics (P-PPA)
=============================================


# 1. Intro

The **Personal Privacy Preserving Analytics (P-PPA)** module has the goal of allowing data analysts and stakeholders to extract useful information from the raw data while preserving the privacy of the users whose data is in the datasets. It leverages concepts like [Differential Privacy](https://en.wikipedia.org/wiki/Differential_privacy) and [K-Anonymity](https://en.wikipedia.org/wiki/K-anonymity) so that data can be processed and shared while guaranteeing privacy for the users

P-PPA includes a set of functionalities that allow perform data operations preserving the major privacy properties: *k-anonymity*, [*z-anonymity*](https://ieeexplore.ieee.org/abstract/document/9378422), *differential privacy*. P-PPA is capable to handle different sources of data inputs, that define which kind of privacy property is called into account: we have design solutions for tabular and batch stream, handled with *PostgreSQL*, *MongoDB*, and *CSV* modules, and live stream data. The figure below depicts the P-PPA architecture.

![image info](./pppa.png)

# 2. Installation

P-PPA is a Python3 module that requires the following libraries, which are also listed in the `requirements.txt` file:

```
aniso8601==9.0.1
certifi==2021.5.30
chardet==3.0.4
click==8.0.1
cycler==0.10.0
diffprivlib==0.3.0
Flask==1.1.2
Flask-RESTful==0.3.8
idna==2.10
itsdangerous==2.0.1
Jinja2==3.0.1
joblib==1.0.1
kiwisolver==1.3.1
MarkupSafe==2.0.1
matplotlib==3.3.3
numpy==1.18.5
pandas==1.1.5
Pillow==8.3.0
psycopg2==2.8.6
pymongo==3.11.2
pyparsing==2.4.7
python-dateutil==2.8.1
pytz==2021.1
PyYAML==5.4.1
requests==2.24.0
scikit-learn==0.24.2
scipy==1.7.0
six==1.16.0
SQLAlchemy==1.3.20
threadpoolctl==2.1.0
urllib3==1.25.11
Werkzeug==2.0.1
zanon==0.3.3
zanon==0.3.2
```

To install the module, it is only necessary to clone this repository and install the dependencies. The testing has been done on a Linux Ubuntu 20 Machine, but the module is supposed to work on any Python installation.

# 3. Usage

The input data need to be in `pandas.Dataframe` format; in these examples, it is supposed to be already present.
The chosen datasets for these examples is called "Adult", available in `data_manage/data_samples` folder. "Credit" and "Diabetes" datasets are also available in the same folder. Detailed API documentation is avaiable in the `documentation/doc_sphinx` folder in the Sphinz format.

Following there are some usage examples:

 **1. K-anonymity, performing Mondrian algorithm.**

 Creating the *Mondrian* class with just the k parameter: all column attributes will be taken into account to perform the Mondrian algorithm.

 ```python
 from algorithms.kanonymity.mondrian.mondrian import Mondrian
 mondrian = Mondrian(3)
 k_anonymized_dataframe = mondrian.perform(input_dataframe)
 ```

 **2. K-anonymity, without performing Mondrian algorithm.**

 Differently from the previous example, here are selected the column indexes 2, 3, 4 and 5, corresponding to *fnlwgt*, *education*, *education-num* and *marital-status* attributes. Having specified these columns, the Mondrian algorithm will not be performed (obviously for this particular data and columns. For further details please check *“perform”* and *“_check_kanon_from_columns”* class methods documentation in the *“mondrian.py”* module).

 ```python
 from algorithms.kanonymity.mondrian.mondrian import Mondrian
 mondrian = Mondrian(3, user_choice_index=[2,3,4,5])
 k_anonymized_dataframe = mondrian.perform(input_dataframe)
 ```

 **3. K-anonymity, select QIs and whitelist columns.**

 In this example the parameters *"qi_indexes"* and *"whitelist"* are used to select respectively:

 - which columns need to be considered as quasi-indetifiers, selected to achieve k-anonymity
 - white-list columns, selected to be displayed among transformed attributes but conserved untouched as they are (they don't contribute to k-anonymity conversation: their use is related to performing anonymization on some attributes and conserving one o more possible labels (the white-listed attributes) to extract statistics and be able to train machine learning models).

 Some more details:

 - if *"qi_indexes"* isn't specified, all columns are considered as quasi-identifiers.
 - if a column index in the *"whitelist"* list is also present in  *"qi_indexes"* one, it's behavior will be overwritten and treated as white-listed.

 Note: this to parameters have been introduced for an administrator use. 

 ```python
 from algorithms.kanonymity.mondrian.mondrian import Mondrian
 mondrian = Mondrian(3, qi_indexes=[1,2,4,6], whitelist=[8,9])
 k_anonymized_dataframe = mondrian.perform(input_dataframe)
 ```

 **4. Differential privacy, performing the mean of the first and third columns.**

 For further details please check *“Dp_IBM_wrapper”* class documentation in the *“dp_IBM_wrapper.py”* module.

 ```python
 from algorithms.differential_privacy.dp_IBM.dp_IBM_wrapper import Dp_IBM_wrapper
 mean = Dp_IBM_wrapper([0,2], "mean", 0.6)
 ret_mean = mean.perform(input_dataframe)
 ```

 **5. Differential privacy, performing the histogram on the first column.**

 For further details please check *“Dp_IBM_wrapper”* class documentation in the *“dp_IBM_wrapper.py”* module.

 ```python
 from algorithms.differential_privacy.dp_IBM.dp_IBM_wrapper import Dp_IBM_wrapper
 histogram = Dp_IBM_wrapper([0], "histogram", 0.6)
 hist, bins = histogram.perform(input_dataframe, bins=6)
 ```

 This is the same output that you would obtain exploiting numpy library. The following code it’s just an example of a way with which you can use this output.

 ```python
 from matplotlib import pyplot as plt
 width = 0.7 * (bins[1] - bins[0])
 center = (bins[:-1] + bins[1:]) / 2
 plt.bar(center, hist, width=width)
 plt.show()
 ```

 **6. Differential privacy, performing the 2d histogram on the first and the third columns.**

 For further details please check *“Dp_IBM_wrapper”* class documentation in the *“dp_IBM_wrapper.py”* module. Please take in mind that the histogram from the first and the third column of *“Adult”* dataset has no semantic meaning.

 ```python
 from algorithms.differential_privacy.dp_IBM.dp_IBM_wrapper import Dp_IBM_wrapper
 histogram2d = Dp_IBM_wrapper([0,2], "histogram2d", 0.6)
 matrix2d, xedge, yedge = histogram2d.perform(input_dataframe)
 ```

 This is the same output that you would obtain exploiting numpy library.

 **7. Z-Anonymity in a Data Stream.**

Z-anonymity is an anonymization property and algorithm for data streams. To use it, you need to have a Data Frame and indicates which columns identify the time, the users and the attributes.

```python
import algorithms.zanon
import pandas as pd

z = algorithms.zanon.zanon(10, 3)
df = pd.read_csv("sample-stream.csv")
anon_df = z.perform(df, "time", "user", "item")

 ```

## Web Service

The P-PPA implement a simpe Web server written in Flask that allow to use the P-PPA as a Web Service, allow any component (written in any language) to use the P-PPA, potentially hosted in a different server. 

To start it, just run:

```
cd restapi
python init_flask.py
```

The Web API are documented in the OpenAPI format at [this](https://gitlab.com/pimcity/wp5/open-api/-/blob/master/WP2/privacy-preserving-analytics.yml) url.

# 4. License

The P-PPA are distributed under AGPL-3.0-only, see the `LICENSE` file.

Copyright (C) 2021  Politecnico di Torino - Martino Trevisan, Marco Mellia,
                                            Nikhil Jha, Giovanni Camarda,
                                            Luca Vassio



