# Build with:
# docker build . --tag martino90/personal-privacy-preserving-analytics
# Push with:
# docker push martino90/personal-privacy-preserving-analytics
# Play with:
# docker run -it --rm --entrypoint /bin/bash martino90/personal-privacy-preserving-analytics

FROM ubuntu:20.04
LABEL maintainer="Martino Trevisan"

RUN apt-get update
RUN apt-get install -y python3 python3-pip git libpq-dev

RUN cd /opt ; git clone https://gitlab.com/pimcity/wp2/personal-privacy-preserving-analytics
RUN cd /opt/personal-privacy-preserving-analytics ; pip3 install -r requirements.txt 

WORKDIR /opt/personal-privacy-preserving-analytics/restapi
ENTRYPOINT ["python3", /opt/personal-privacy-preserving-analytics/restapi/init_flask.py"]