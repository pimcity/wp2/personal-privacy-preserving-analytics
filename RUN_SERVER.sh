#!/bin/bash

source pppa/bin/activate

cd restapi
python init_flask.py  

# TEST WITH:
# curl -k -X 'GET'   'https://easypims.pimcity-h2020.eu/pppa/kanon?token=pimcity2021&k=2&data_source=csv&qi_indexes=1&qi_indexes=2&qi_indexes=3&csv_path=%2Fhome%2Fmartino%2Fpersonal-privacy-preserving-analytics%2Fsample-dataset.csv'   -H 'accept: application/json' | jq -r .  | python3 -c "import pandas as pd ; import json; import sys ; df=pd.DataFrame(json.loads(sys.stdin.read())); print(df)"

#curl -k -X 'GET'   'https://easypims.pimcity-h2020.eu/pppa/diffpriv?token=pimcity2021&epsilon=1&column_indexes=3&query=mean&data_source=csv&dtype=float&csv_path=%2Fhome%2Fmartino%2Fpersonal-privacy-preserving-analytics%2Fsample-dataset.csv'  -H 'accept: application/json'

# curl -k -X 'GET'   'https://easypims.pimcity-h2020.eu/pppa/zanon?token=pimcity2021&zeta=3&deltat=10&data_source=csv&csv_path=%2Fhome%2Fmartino%2Fpersonal-privacy-preserving-analytics%2Fsample-stream.csv&time_col=time&user_col=user&attribute_col=item'  -H 'accept: application/json' | jq -r .  | python3 -c "import pandas as pd ; import json; import sys ; df=pd.DataFrame(json.loads(sys.stdin.read())); print(df)"
