"""
This module takes care of the connection to a Mongo database and the data reading from it.
"""

from pymongo import MongoClient, errors
import pandas as pd
from pandas import errors
import json
from bson import json_util
from statistics import mean


class MongoDB:
    """
    You must instantiate this class to establish a connection with a MongoDB database.

    Attributes:
        host_name (str): Hostname to connect to MongoDB, e.g. "localhost".
        port (int): Port number to connect to MongoDB, e.g. 27017.
        username (str, optional): Username to connect to MongoDB, it's optional.
        password (str, optional): Password to connect to MongoDB, it's optional.
        db_name (str): Database name to connect to MongoDB, e.g. "adult".
    """
    def __init__(self, host_name, port, username, password, db_name):
        self.host_name = host_name
        self.port = port
        self.username = username
        self.password = password
        self.db_name = db_name

    def _connect_mongo(self):
        """
        Connects to a Mongo database, given host_name, port, db_name and optionally username and password.

        Returns:
            :obj:`pymongo.database.Database`: Handle to the connected database.
        """

        if self.username and self.password:
            mongo_uri = 'mongodb://%s:%s@%s:%s/%s' % (
                self.username, self.password, self.host_name, self.port, self.db_name)
            conn = MongoClient(mongo_uri)
        else:
            conn = MongoClient(self.host_name, self.port)
        return conn[self.db_name]

    def read_data(self, collection, no_id=True):
        """
        Reads from a Mongo database after the connection, and stores data into a DataFrame.

        Args:
            collection (str): Mongo collection name from which retrieve the data.
            no_id (bool, optional): If is True, the ids are removed from the final dataframe.

        Returns:
            :obj:`pandas.DataFrame`: Output dataframe.
        """

        db = self._connect_mongo()
        from_mongo = False

        cursor = db[collection].find({})
        cursor = list(cursor)
        if not cursor:
            raise errors.CollectionInvalid
        cursor.pop()
        for el in cursor:
            if any(isinstance(i,dict) for i in el.values()) or any(isinstance(i,list) for i in el.values()):
                from_mongo = True
            if from_mongo:
                break

        if from_mongo:
            #check if is present a multiple dict or list level
            for el in cursor:
                for i in el.values():
                    if isinstance(i, dict):
                        for j in i.values():
                            if isinstance(j, dict) or isinstance(j, list):
                                raise errors.ParserError
                    if isinstance(i, list):
                        for j in i:
                            if isinstance(j, dict) or isinstance(j, list):
                                raise errors.ParserError

            tmp = json.dumps(list(cursor), sort_keys=True, indent=4, default=json_util.default)
            data = json.loads(tmp)

            for row in data:
                for key, value in row.items():
                    if isinstance(value, list):
                        if isinstance(value[0], str):
                            tmp = '_'.join(str(e) for e in value)
                        else:
                            tmp = mean(value)
                        row[key] = tmp


            data = pd.json_normalize(data)
            df = data
        else:
            df = pd.DataFrame(list(cursor))
        if no_id:
            col_list = [col for col in df.columns if col.startswith("_id")]
            for el in col_list:
                del df[el]


        return df
