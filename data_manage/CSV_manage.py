"""
This module takes care of the data retrieving from a CSV file.
"""

import pandas as pd


def read_from_csv(input_path, **other_args):
    """
    This function acts as a mapping for the original pandas "read_csv" method.

    Args:
        input_path (str): The input csv path.
        **other_args: These arguments are the same of the pandas "read_csv" method, so this function can be used like
            it; just specify the further arguments you want to use and their values.

    Returns:
        :obj:`pandas.DataFrame`: The returned dataframe.
    """
    p = {"sep": ',', "delimiter": None, "header": 'infer', "names": None, "index_col": None, "usecols": None,
         "squeeze": False, "prefix": None, "mangle_dupe_cols": True, "dtype": None, "engine": None, "converters": None,
         "true_values": None, "false_values": None, "skipinitialspace": False, "skiprows": None, "skipfooter": 0,
         "nrows": None, "na_values": None, "keep_default_na": True, "na_filter": True, "verbose": False,
         "skip_blank_lines": True, "parse_dates": False, "infer_datetime_format": False, "keep_date_col": False,
         "date_parser": None, "dayfirst": False, "cache_dates": True, "iterator": False, "chunksize": None,
         "compression": 'infer', "thousands": None, "decimal": '.', "lineterminator": None, "quotechar": '"',
         "quoting": 0, "doublequote": True, "escapechar": None, "comment": None, "encoding": None, "dialect": None,
         "error_bad_lines": True, "warn_bad_lines": True, "delim_whitespace": False, "low_memory": True,
         "memory_map": False,
         "float_precision": None}

    for key, value in other_args.items():
        p[key] = value
    return pd.read_csv(input_path, sep=p["sep"], delimiter=p["delimiter"], header=p["header"], names=p["names"],
                       index_col=p["index_col"], usecols=p["usecols"], squeeze=p["squeeze"], prefix=p["prefix"],
                       mangle_dupe_cols=p["mangle_dupe_cols"], dtype=p["dtype"], engine=p["engine"],
                       converters=p["converters"], true_values=p["true_values"], false_values=p["false_values"],
                       skipinitialspace=p["skipinitialspace"], skiprows=p["skiprows"], skipfooter=p["skipfooter"],
                       nrows=p["nrows"], na_values=p["na_values"], keep_default_na=p["keep_default_na"],
                       na_filter=p["na_filter"], verbose=p["verbose"], skip_blank_lines=p["skip_blank_lines"],
                       parse_dates=p["parse_dates"], infer_datetime_format=p["infer_datetime_format"],
                       keep_date_col=p["keep_date_col"], date_parser=p["date_parser"], dayfirst=p["dayfirst"],
                       cache_dates=p["cache_dates"], iterator=p["iterator"], chunksize=p["chunksize"],
                       compression=p["compression"], thousands=p["thousands"], decimal=p["decimal"],
                       lineterminator=p["lineterminator"], quotechar=p["quotechar"], quoting=p["quoting"],
                       doublequote=p["doublequote"], escapechar=p["escapechar"], comment=p["comment"],
                       encoding=p["encoding"], dialect=p["dialect"], error_bad_lines=p["error_bad_lines"],
                       warn_bad_lines=p["warn_bad_lines"], delim_whitespace=p["delim_whitespace"],
                       low_memory=p["low_memory"], memory_map=p["memory_map"], float_precision=p["float_precision"])
