"""
This module contains methods to manipulate and sanitize dataframes. Please put here functionalities you need according
to different dataframes you have to handle.
"""

import numpy as np


def sanitize_df(df):
    """
    Removes spaces from dataframe 'cells' and removes a row if at least one of its values is corrupted.

    Args:
        df (:obj:`pandas.DataFrame`): input dataframe to be sanitized.

    Returns:
        :obj:`pandas.DataFrame`: sanitized dataframe.
    """
    try:
        df_tmp = df.copy()
        types = df_tmp.dtypes
        att_name = [column for column in df_tmp.columns]
        masked_cat = [cat for t, cat in zip(types, att_name) if t != "int64" and t != "float64"]
        for cat in masked_cat:
            df_tmp[cat] = df_tmp[cat].str.replace('^ +| +$', '').str.replace("?", " ")
        for column in df_tmp:
            df_tmp[column].replace(" ", np.nan, inplace=True)
        for column in df_tmp:
            df_tmp.dropna(subset=[column], inplace=True)
        if df_tmp.empty:
            return df
    except:
        return df
    return df_tmp
