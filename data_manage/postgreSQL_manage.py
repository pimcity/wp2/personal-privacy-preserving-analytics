"""
This module takes care of the connection to a postgreSQL database and the data reading from it.
"""

import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import pandas as pd
from sqlalchemy import create_engine


class PostgreSQL:
    """
    You must instantiate this class to establish a connection with a PostgreSQL database.

    Attributes:
        server_name_connect (str): Server name of a postgreSQL server, e.g. "postgres".
        server_user_name(str): Username for a postgreSQL server, e.g. "postgres".
        server_user_password (str): Password for a postgreSQL server, e.g. "provapost".
        db_name_create (str): Database name of a postgreSQL server, e.g. "adut_data".
        server_host (str): Server host name of a postgreSQL server, e.g. "localhost".
        server_port (str): Server port of a postgreSQL server, e.g. "5432".

    """
    def __init__(self, server_name_connect, server_user_name, server_user_password, db_name_create, server_host,
                 server_port):
        """
        The _check_database private method is called here, during class initialization.
        """
        self.server_name_connect = server_name_connect
        self.server_user_name = server_user_name
        self.server_user_password = server_user_password
        self.db_name_create = db_name_create
        self.server_host = server_host
        self.server_port = str(server_port)
        self.cur = None
        self._check_database()

    def _check_database(self):
        """
        It checks if the database corresponding to the given class input parameters already exists, if not it will raised
        an exception.

        Returns:
            None: The database cursor is linked in the 'self.cur' class attribute.
        Raises:
            OperationalError.
        """

        con = psycopg2.connect(dbname=self.server_name_connect, user=self.server_user_name, host='',
                               password=self.server_user_password)
        con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        self.cur = con.cursor()
        self.cur.execute(sql.SQL("SELECT 1 FROM pg_catalog.pg_database WHERE datname = %s"), [self.db_name_create])
        exists = self.cur.fetchone()
        if not exists:
            raise psycopg2.OperationalError("Requested database does not exist!")

    def retrieve_table(self, table_name):
        """
        Reads from a postgreSQL database using the database cursor after the connection, and stores data into a
        DataFrame.

        Args:
            table_name (str): String name of the requested table.

        Returns:
            :obj:`pandas.DataFrame`: Output dataframe.
        """
        engine = create_engine('postgresql://' + self.server_user_name + ':' + self.server_user_password + '@' +
                               self.server_host + ':' + self.server_port + '/' + self.db_name_create)
        df = pd.read_sql_table(table_name, con=engine)
        return df