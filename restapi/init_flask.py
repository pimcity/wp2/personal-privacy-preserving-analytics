from flask import Flask, Response
from flask_restful import Api, Resource, reqparse
import pandas as pd
import numpy as np
import yaml
import json
import logging
from pydoc import locate
import sys
sys.path.insert(0, '../')
from algorithms.kanonymity.mondrian.mondrian import Mondrian
import algorithms.zanon
from algorithms.differential_privacy.dp_IBM.dp_IBM_wrapper import Dp_IBM_wrapper
from data_manage.dataframe_utility import sanitize_df
from data_manage.CSV_manage import read_from_csv
from data_manage.mongoDB_manage import MongoDB
from data_manage.postgreSQL_manage import PostgreSQL

app = Flask(__name__)
api = Api(app)

"""
This is the entry point of the pppa pipeline. It's a flask implementation that takes care of the requests for 
k-anonymity, z-anonymity and differential privacy analytics. It maps all the needed parameters according to the requested query.
YAML file content:
- Host address and port
- File name for the log file
- List of allowed tokens

General rules for parameters:
- for these attributes list type is needed also for passing a single value: user_choice_index, qi_indexes, whitelist column_indexes, bins, weights
- instead of True or False, please use 1 or 0
- bounds and axis attributes need list types, then will be transformed in tuple of tuples of integers as required for 
IBM functions. Bounds parameters is structured in the form of a tuple that contains two tuples of n integers, coherently 
with the number of ranges needed according to other parameters like column_indexes  and keepdims. The two internal 
tuples contain respectively al the mnìinimun and maximum values.
- for dtype attribute just specify the python built-in type, non-putting it in a string object
- query attribute can have the values specified in the Dp_IBM_wrapper documentation
- the original IBM differential privacy library comprehends an "accountant" class, whose objects can be passed to functions. 
In our view, this type of duties is embraced by other modules, for this reason, this class object is not considered in the possible parameters set.


"""

class ZanonCommand(Resource):
    """
    This class takes care of the z-anonymity requests.

    Attributes, please refer to Zanon, CSV_manage, mongoDB_setup, postgreSQL_manage documentation:
        put_args (:obj:`flask_restful.reqparse.RequestParser`): object to parse all the possible arguments:
        - authentication: token
        - z-anonymity: deltat, zeta, time_col, user_col, attribute_col
        - data retrieve: data_source, csv_path, server_data, host_data, port_data, user_data, pass_data, db_data, table_coll_data

    """
    def __init__(self):
        self.put_args = reqparse.RequestParser(bundle_errors=True)
        self.put_args.add_argument("token", action='append', required=True)
        self.put_args.add_argument("deltat", action='append', required=True)
        self.put_args.add_argument("zeta", action='append', required=True)
        self.put_args.add_argument("time_col", action='append', required=True)
        self.put_args.add_argument("user_col", action='append', required=True)
        self.put_args.add_argument("attribute_col", action='append', required=True)
        self.put_args.add_argument("data_source", action='append', required=True)
        self.put_args.add_argument("csv_path", action='append')
        self.put_args.add_argument("server_data", action='append')
        self.put_args.add_argument("host_data", action='append')
        self.put_args.add_argument("port_data", action='append')
        self.put_args.add_argument("user_data", action='append')
        self.put_args.add_argument("pass_data", action='append')
        self.put_args.add_argument("db_data", action='append')
        self.put_args.add_argument("table_coll_data", action='append')

    def get(self):
        """
        This methods responds to the "/zanon" command.

        Returns:
            :obj:`Union` [None, :obj:`list` of :obj:`list` of :obj:`dict` of (string, string), `flask.wrappers.Response`]:
            the returned JSON format object is a string corresponding to a list of lists (one for each dataframe row)
            of dictionaries string - string (one dict for each attribute/value interpolation) in the form of "attr1":"Jon".
            If the returned object is a `flask.wrappers.Response` the possible codes are the standard HTTP ones,
            clarifying that HTTP 401 Bad Request is used if the requested query needs to retrieve data from CSV while
            non specifying the correct path, HTTP 415 Unsupported Media Type if it is requested an unexpected data source
            or after some error retrieving data and HTTP 401 Unauthorized if the query token isn't present in the allowed
            tokens list.
        """

        args = self.put_args.parse_args()
        tok = args["token"][0]
        if tok in toks:
            deltat= int(args["deltat"][0])
            zeta= int(args["zeta"][0])
            time_col= args["time_col"][0]
            user_col= args["user_col"][0]
            attribute_col= args["attribute_col"][0]

            server_data = args["server_data"][0] if args["server_data"] else None
            host_data = args["host_data"][0] if args["host_data"] else None
            port_data = int(args["port_data"][0]) if args["port_data"] else None
            user_data = args["user_data"][0] if args["user_data"] else ""
            pass_data = args["pass_data"][0] if args["pass_data"] else ""
            db_data = args["db_data"][0] if args["db_data"] else None
            table_coll_data = args["table_coll_data"][0] if args["table_coll_data"] else None

            df = pd.DataFrame()
            data_source = args["data_source"][0] if args["data_source"] else None
            if data_source == "csv":
                path = args["csv_path"][0] if args["csv_path"] else None
                if not path: return Response(status=400, mimetype='text/plain') #Bad Request
                df = read_from_csv(path, skipinitialspace=True)
            elif data_source == "mongodb":
                mongo = MongoDB(host_data, port_data, user_data, pass_data, db_data)
                df = mongo.read_data(table_coll_data)
            elif data_source == "postgre":
                postgre = PostgreSQL(server_data, user_data, pass_data, db_data, host_data, port_data)
                df = postgre.retrieve_table(table_coll_data)
            else:
                return Response(status=415, mimetype='text/plain') #Unsupported Media Type
            if df.empty:
                return Response(status=415, mimetype='text/plain')  # Unsupported Media Type
            sanitized_df = sanitize_df(df)
            z = algorithms.zanon.zanon(deltat, zeta)

            anonymized_df = z.perform(sanitized_df, time_col, user_col, attribute_col)
            result = anonymized_df.to_json(orient='records')
            return result
        else:
            return Response(status=401, mimetype='text/plain') #Unauthorized



class KanonCommand(Resource):
    """
    This class takes care of the k-anonymity requests.

    Attributes, please refer to Mondrian, CSV_manage, mongoDB_setup, postgreSQL_manage documentation:
        put_args (:obj:`flask_restful.reqparse.RequestParser`): object to parse all the possible arguments:
        - authentication: token
        - k-anonymity: k, user_choice_index, qi_indexes, whitelist
        - data retrieve: data_source, csv_path, server_data, host_data, port_data, user_data, pass_data, db_data, table_coll_data

    """
    def __init__(self):
        self.put_args = reqparse.RequestParser(bundle_errors=True)
        self.put_args.add_argument("token", action='append', required=True)
        self.put_args.add_argument("k", action='append', required=True)
        self.put_args.add_argument("user_choice_index", action='append')
        self.put_args.add_argument("qi_indexes", action='append')
        self.put_args.add_argument("whitelist", action='append')
        self.put_args.add_argument("data_source", action='append', required=True)
        self.put_args.add_argument("csv_path", action='append')
        self.put_args.add_argument("server_data", action='append')
        self.put_args.add_argument("host_data", action='append')
        self.put_args.add_argument("port_data", action='append')
        self.put_args.add_argument("user_data", action='append')
        self.put_args.add_argument("pass_data", action='append')
        self.put_args.add_argument("db_data", action='append')
        self.put_args.add_argument("table_coll_data", action='append')

    def get(self):
        """
        This methods responds to the "/kanon" command.

        Returns:
            :obj:`Union` [None, :obj:`list` of :obj:`list` of :obj:`dict` of (string, string), `flask.wrappers.Response`]:
            the returned JSON format object is a string corresponding to a list of lists (one for each dataframe row)
            of dictionaries string - string (one dict for each attribute/value interpolation) in the form of "attr1":"Jon".
            If the returned object is a `flask.wrappers.Response` the possible codes are the standard HTTP ones,
            clarifying that HTTP 401 Bad Request is used if the requested query needs to retrieve data from CSV while
            non specifying the correct path, HTTP 415 Unsupported Media Type if it is requested an unexpected data source
            or after some error retrieving data and HTTP 401 Unauthorized if the query token isn't present in the allowed
            tokens list.
        """

        args = self.put_args.parse_args()
        tok = args["token"][0]
        if tok in toks:
            k = int(args["k"][0])
            user_choice_index = args["user_choice_index"] if args["user_choice_index"] else None
            user_choice_index = list(map(int, user_choice_index)) if user_choice_index else None
            qi_indexes = args["qi_indexes"] if args["qi_indexes"] else None
            qi_indexes = list(map(int, qi_indexes)) if qi_indexes else None
            whitelist = args["whitelist"] if args["whitelist"] else None
            whitelist = list(map(int, whitelist)) if whitelist else None

            server_data = args["server_data"][0] if args["server_data"] else None
            host_data = args["host_data"][0] if args["host_data"] else None
            port_data = int(args["port_data"][0]) if args["port_data"] else None
            user_data = args["user_data"][0] if args["user_data"] else ""
            pass_data = args["pass_data"][0] if args["pass_data"] else ""
            db_data = args["db_data"][0] if args["db_data"] else None
            table_coll_data = args["table_coll_data"][0] if args["table_coll_data"] else None

            df = pd.DataFrame()
            data_source = args["data_source"][0] if args["data_source"] else None
            if data_source == "csv":
                path = args["csv_path"][0] if args["csv_path"] else None
                if not path: return Response(status=400, mimetype='text/plain') #Bad Request
                df = read_from_csv(path, skipinitialspace=True)
            elif data_source == "mongodb":
                mongo = MongoDB(host_data, port_data, user_data, pass_data, db_data)
                df = mongo.read_data(table_coll_data)
            elif data_source == "postgre":
                postgre = PostgreSQL(server_data, user_data, pass_data, db_data, host_data, port_data)
                df = postgre.retrieve_table(table_coll_data)
            else:
                return Response(status=415, mimetype='text/plain') #Unsupported Media Type
            if df.empty:
                return Response(status=415, mimetype='text/plain')  # Unsupported Media Type
            sanitized_df = sanitize_df(df)
            mondrian = Mondrian(k, user_choice_index=user_choice_index, qi_indexes=qi_indexes, whitelist=whitelist)
            k_anonymized_df = mondrian.perform(sanitized_df)
            result = k_anonymized_df.to_json(orient='records')
            return result
        else:
            return Response(status=401, mimetype='text/plain') #Unauthorized

class DiffPrivCommand(Resource):
    """
    This class takes care of the IBM differential privacy requests.

    Attributes, please refer to Dp_IBM_wrapper, CSV_manage, mongoDB_setup, postgreSQL_manage documentation:
        put_args (:obj:`flask_restful.reqparse.RequestParser`): object to parse all the possible arguments:
        - authentication: token
        - IBM differential privacy: column_indexes, query, epsilon, bounds, axis, dtype, keepdims, bins, weights, density
        - data retrieve: data_source, csv_path, server_data, host_data, port_data, user_data, pass_data, db_data, table_coll_data

    """
    def __init__(self):
        self.put_args = reqparse.RequestParser(bundle_errors=True)
        self.put_args.add_argument("token", action='append', required=True)
        self.put_args.add_argument("column_indexes", action='append', required=True)
        self.put_args.add_argument("query", action='append', required=True)
        self.put_args.add_argument("epsilon", action='append', required=True)
        self.put_args.add_argument("bounds", action='append')
        self.put_args.add_argument("axis", action='append')
        self.put_args.add_argument("dtype", action='append')
        self.put_args.add_argument("keepdims", action='append')
        self.put_args.add_argument("bins", action='append')
        self.put_args.add_argument("weights", action='append')
        self.put_args.add_argument("density", action='append')
        self.put_args.add_argument("data_source", action='append', required=True)
        self.put_args.add_argument("csv_path", action='append')
        self.put_args.add_argument("server_data", action='append')
        self.put_args.add_argument("host_data", action='append')
        self.put_args.add_argument("port_data", action='append')
        self.put_args.add_argument("user_data", action='append')
        self.put_args.add_argument("pass_data", action='append')
        self.put_args.add_argument("db_data", action='append')
        self.put_args.add_argument("table_coll_data", action='append')

    def get(self):
        """
        This methods responds to the "/diffpriv" command.

        Returns:
            :obj:`Union` [None, str, :obj:`list` of str, `flask.wrappers.Response`]: the string returned object corresponds
            to the normal statistical operations excluding the histograms: these are string conversion of numpy.simpletype
            (for example numpy.float64) or numpy.advancedtype (for example numpy arrays) according to the specifically requested
            operation. If the requested query is a "histogram2d" the returned type is a JSON list of three strings representing
            the matrix2d, xedge and yedge. If the requested query is a "histogram" or a "histogramdd" the returned type
            is a JSON list of two strings representing the hist/matrixdd and the bins/edges. If the returned object is a
            `flask.wrappers.Response` the possible codes are the standard HTTP ones, clarifying that HTTP 401 Bad Request
            is used if the requested query needs to retrieve data from CSV while non specifying the correct path, HTTP
            415 Unsupported Media Type if it is requested an unexpected data source or after some error retrieving data
            and HTTP 401 Unauthorized if the query token isn't present in the allowed tokens list.
        """
        args = self.put_args.parse_args()
        tok = args["token"][0]
        if tok in toks:
            column_indexes = args["column_indexes"] if args["column_indexes"] else None
            column_indexes = list(map(int, column_indexes)) if column_indexes else None
            query = args["query"][0] if args["query"] else None
            epsilon = float(args["epsilon"][0]) if args["epsilon"] else None
            if args["bounds"]:
                bounds = args["bounds"]
                if len(bounds) >= 2:
                    half = len(bounds) // 2
                    bounds = tuple([tuple(bounds[:half]), tuple(bounds[half:])])
                else:
                    bounds = tuple(map(int, bounds))
            else:
                bounds = None
            axis = tuple(map(int, args["axis"])) if args["axis"] else None
            dtype = locate(args["dtype"][0].split(" ")[-1][1:-2]) if args["dtype"] else None
            keepdims = int(args["keepdims"][0]) if args["keepdims"] else False
            if args["bins"]:
                bins = list(map(int, args["bins"]))
                if len(bins) == 1:
                    bins = bins[0]
            else:
                bins = None

            weights = args["weights"] if args["weights"] else None
            weights = list(map(float, weights)) if weights else None
            density = int(args["density"][0]) if args["density"] else None

            server_data = args["server_data"][0] if args["server_data"] else None
            host_data = args["host_data"][0] if args["host_data"] else None
            port_data = int(args["port_data"][0]) if args["port_data"] else None
            user_data = args["user_data"][0] if args["user_data"] else ""
            pass_data = args["pass_data"][0] if args["pass_data"] else ""
            db_data = args["db_data"][0] if args["db_data"] else None
            table_coll_data = args["table_coll_data"][0] if args["table_coll_data"] else None

            df = pd.DataFrame()
            data_source = args["data_source"][0] if args["data_source"] else None
            if data_source == "csv":
                path = args["csv_path"][0] if args["csv_path"] else None
                if not path: return Response(status=400, mimetype='text/plain')  # Unsupported Media Type
                df = read_from_csv(path, skipinitialspace=True)
            elif data_source == "mongodb":
                mongo = MongoDB(host_data, port_data, user_data, pass_data, db_data)
                df = mongo.read_data(table_coll_data)
            elif data_source == "postgre":
                postgre = PostgreSQL(server_data, user_data, pass_data, db_data, host_data, port_data)
                df = postgre.retrieve_table(table_coll_data)
            else:
                return Response(status=415, mimetype='text/plain')  # Unsupported Media Type
            if df.empty:
                return Response(status=415, mimetype='text/plain')  # Unsupported Media Type
            sanitized_df = sanitize_df(df)

            operation = Dp_IBM_wrapper(column_indexes, query, epsilon)
            if query == "histogram" or query == "histogramdd":
                ret1, ret2 = operation.perform(sanitized_df, bounds=bounds, bins=bins, weights=weights, density=density)
                ret = [json.dumps(ret1.tolist()), json.dumps(ret2.tolist())]
                return ret
            elif query == "histogram2d":
                ret1, ret2, ret3 = operation.perform(sanitized_df, bounds=bounds, bins=bins, weights=weights, density=density)
                ret = [json.dumps(ret1.tolist()), json.dumps(ret2.tolist()), json.dumps(ret3.tolist())]
                return ret
            else:
                ret = operation.perform(sanitized_df, bounds=bounds, axis=axis, dtype=dtype, keepdims=keepdims)
                if isinstance(ret, np.ndarray):
                    ret = json.dumps(ret.tolist())
                    return ret
                else:
                    return str(ret)

        else:
            return Response(status=401, mimetype='text/plain') #Unauthorized


if __name__ == "__main__":
    with open("settings.yaml", 'r') as stream:
        res = yaml.safe_load(stream)
        log_path = res["content"]["log_file_name"]
        toks = res["content"]["tokens"]
        address = res["content"]["address"]

    logging.raiseExceptions = False
    logging.basicConfig(filename=log_path, level=logging.DEBUG)

    api.add_resource(KanonCommand, "/kanon")
    api.add_resource(DiffPrivCommand, "/diffpriv")
    api.add_resource(ZanonCommand, "/zanon")
    app.run(host=address["host"], port=address["port"], debug=True)
