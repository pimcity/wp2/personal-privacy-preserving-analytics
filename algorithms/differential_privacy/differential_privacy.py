"""
This module contains a sort of interface for different differential privacy algorithms.
"""

class Differential_privacy:
    """
    Differential_privacy class acts as a superclass for all the various algorithms that can be chosen to perform
    differential privacy queries. An inheritance over a pseudo-interface approach was preferred to ease the
    implementation of common functionalities.

    Attributes:
        dispatcher (:obj:`dict` of str:function): the dictionary that maps a string, corresponding to the desired query
            to be performed in a differential privacy manner, with the function pointer that has to be called to execute
            that query. In this class it's just defined as an empty dictionary. It will be extended in the derived class
            'Dp_IBM_wrapper'.
    """
    def __init__(self):
        self.dispatcher = {}

    def perform(self):
        """
        This method needs to be extended to actually perform differential privacy.
        """
        print("Starting differential privacy algorithm")
