"""
This module is a wrapper to set up and perform the actual functions for differential privacy imported from the
differential privacy library by IBM; it follows the official repo:
https://github.com/IBM/differential-privacy-library.git.
"""


from algorithms.differential_privacy.differential_privacy import Differential_privacy
from algorithms.differential_privacy.dp_IBM.dp_functions import dp_sum
from algorithms.differential_privacy.dp_IBM.dp_functions import dp_nansum
from algorithms.differential_privacy.dp_IBM.dp_functions import dp_mean
from algorithms.differential_privacy.dp_IBM.dp_functions import dp_nanmean
from algorithms.differential_privacy.dp_IBM.dp_functions import dp_var
from algorithms.differential_privacy.dp_IBM.dp_functions import dp_nanvar
from algorithms.differential_privacy.dp_IBM.dp_functions import dp_std
from algorithms.differential_privacy.dp_IBM.dp_functions import dp_nanstd
from algorithms.differential_privacy.dp_IBM.dp_functions import dp_histogram
from algorithms.differential_privacy.dp_IBM.dp_functions import dp_histogram2d
from algorithms.differential_privacy.dp_IBM.dp_functions import dp_histogramdd
import warnings


class Dp_IBM_wrapper(Differential_privacy):
    """
    This class extends the Differential_privacy class.
    Supported queries: sum, nansum, mean, nanmean, var, nanvar, std, nanstd, histogram, histogram2d, histogramdd.

    Attributes:
        column_indexes (:obj:`list` of int): The list containing one or more integers corresponding to the dataframe
            columns over which perform the requested query.
        query (str): The requested query. Check 'dispatcher' keys to be aware of the currently supported queries.
        epsilon (float): The epsilon parameter for the differential privacy query.

        dispatcher (:obj:`dict` of (str, function)): For further explanation, please refer to the 'dispatcher' attribute,
            in the super class 'Differential_privacy'. The dispatcher is set up; if some functionalities have to be
            added, it is just necessary to import the required method and add it as the value of the new query string
            thanks to which it will be called.
    """
    def __init__(self, column_indexes, query, epsilon):
        Differential_privacy.__init__(self)
        self.column_indexes = column_indexes
        self.query = query
        self.epsilon = epsilon
        self.dispatcher = {
            'sum': dp_sum,
            'nansum': dp_nansum,
            'mean': dp_mean,
            'nanmean': dp_nanmean,
            'var': dp_var,
            'nanvar': dp_nanvar,
            'std': dp_std,
            'nanstd': dp_nanstd,
            'histogram': dp_histogram,
            'histogram2d': dp_histogram2d,
            'histogramdd': dp_histogramdd
        }

    def perform(self, df, **other_args):
        """
        This method extends the one present in the Differential_privacy class. The right function is called from the
        dispatcher.

        Args:
            df (:obj:`pandas.DataFrame`): Input dataframe to query for the requested operation in a differential
                privacy flavour.
            **other_args: other arguments are accepted to reflect IBM library methods. These will be passed to IBM
                methods and there handled.

        Returns:
            :obj:`Union` [:obj:`tuple`, :obj:`numpy.type`]: the return type depend on the desired query, it depends on what
            the IBM method returns.

        Raises:
            TypeError: the argument 'column_indexes' must be a list, raised this exception if not.
            ValueError: raises this exception if the number of columns are not coherent with the requested operation.
        """
        super(Dp_IBM_wrapper, self).perform()
        print('Performing operation: ' + self.query)

        warnings.simplefilter('ignore')
        if type(self.column_indexes) != list:
            raise TypeError("The argument 'column_indexes' must be a list")
        if self.query == "histogram2d":
            if len(self.column_indexes) != 2:
                raise ValueError("Submitted query must select exactly two columns as argument!")
        return self.dispatcher[self.query](df, self.column_indexes, self.epsilon, **other_args)
