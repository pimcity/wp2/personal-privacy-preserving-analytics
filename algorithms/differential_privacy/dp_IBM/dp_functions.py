"""
This module contains one wrapper method for each one of the imported methods from the differential privacy IBM library.
The supported functionalities are: sum, nansum, mean, nanmean, var, nanvar, std, nanstd, histogram, histogram2d,
histogramdd.
For further documentation please refer to the IBM repo: https://github.com/IBM/differential-privacy-library/tree/main/diffprivlib/tools.
"""

from diffprivlib.tools.utils import sum
from diffprivlib.tools.utils import nansum
from diffprivlib.tools.utils import mean
from diffprivlib.tools.utils import nanmean
from diffprivlib.tools.utils import var
from diffprivlib.tools.utils import nanvar
from diffprivlib.tools.utils import std
from diffprivlib.tools.utils import nanstd
from diffprivlib.tools.histograms import histogram
from diffprivlib.tools.histograms import histogram2d
from diffprivlib.tools.histograms import histogramdd


def dp_sum(df, column_indexes, epsilon, bounds=None, accountant=None, axis=None, dtype=None, keepdims=False):
    columns = df[df.columns[column_indexes]]
    data = [columns[el].tolist() for el in columns]
    return sum(array=data, epsilon=epsilon, bounds=bounds, accountant=accountant, axis=axis,
               dtype=dtype, keepdims=keepdims)


def dp_nansum(df, column_indexes, epsilon, bounds=None, accountant=None, axis=None, dtype=None, keepdims=False):
    columns = df[df.columns[column_indexes]]
    data = [columns[el].tolist() for el in columns]
    return nansum(array=data, epsilon=epsilon, bounds=bounds, accountant=accountant, axis=axis,
                  dtype=dtype, keepdims=keepdims)


def dp_mean(df, column_indexes, epsilon, bounds=None, accountant=None, axis=None, dtype=None, keepdims=False):
    columns = df[df.columns[column_indexes]]
    data = [columns[el].tolist() for el in columns]
    return mean(array=data, epsilon=epsilon, bounds=bounds, axis=axis, dtype=dtype,
                keepdims=keepdims, accountant=accountant)


def dp_nanmean(df, column_indexes, epsilon, bounds=None, accountant=None, axis=None, dtype=None, keepdims=False):
    columns = df[df.columns[column_indexes]]
    data = [columns[el].tolist() for el in columns]
    return nanmean(array=data, epsilon=epsilon, bounds=bounds, axis=axis, dtype=dtype,
                   keepdims=keepdims, accountant=accountant)


def dp_var(df, column_indexes, epsilon, bounds=None, accountant=None, axis=None, dtype=None, keepdims=False):
    columns = df[df.columns[column_indexes]]
    data = [columns[el].tolist() for el in columns]
    return var(array=data, epsilon=epsilon, bounds=bounds, axis=axis, dtype=dtype,
               keepdims=keepdims, accountant=accountant)


def dp_nanvar(df, column_indexes, epsilon, bounds=None, accountant=None, axis=None, dtype=None, keepdims=False):
    columns = df[df.columns[column_indexes]]
    data = [columns[el].tolist() for el in columns]
    return nanvar(array=data, epsilon=epsilon, bounds=bounds, axis=axis, dtype=dtype,
                  keepdims=keepdims, accountant=accountant)


def dp_std(df, column_indexes, epsilon, bounds=None, accountant=None, axis=None, dtype=None, keepdims=False):
    columns = df[df.columns[column_indexes]]
    data = [columns[el].tolist() for el in columns]
    return std(array=data, epsilon=epsilon, bounds=bounds, axis=axis, dtype=dtype,
               keepdims=keepdims, accountant=accountant)


def dp_nanstd(df, column_indexes, epsilon, bounds=None, accountant=None, axis=None, dtype=None, keepdims=False):
    columns = df[df.columns[column_indexes]]
    data = [columns[el].tolist() for el in columns]
    return nanstd(array=data, epsilon=epsilon, bounds=bounds, axis=axis, dtype=dtype,
                  keepdims=keepdims, accountant=accountant)


def dp_histogram(df, column_indexes, epsilon, bounds=None, bins=10, weights=None, density=None, accountant=None):
    columns = df[df.columns[column_indexes]]
    data = [columns[el].tolist() for el in columns]
    if not bins:
        bins = 10
    return histogram(sample=data, epsilon=epsilon, bins=bins, range=bounds, weights=weights,
                     density=density, accountant=accountant)


def dp_histogram2d(df, column_indexes, epsilon, bounds=None, bins=10, weights=None, density=None, accountant=None):
    data = [df[df.columns[i]] for i in column_indexes]
    if not bins:
        bins = 10
    return histogram2d(array_x=data[0], array_y=data[1], epsilon=epsilon, bins=bins, range=bounds,
                       weights=weights, density=density, accountant=accountant)


def dp_histogramdd(df, column_indexes, epsilon, bounds=None, bins=10, weights=None, density=None, accountant=None):
    columns = df[df.columns[column_indexes]]
    data = [columns[el].tolist() for el in columns]
    if not bins:
        bins = 10
    return histogramdd(sample=data, epsilon=epsilon, bins=bins, range=bounds, weights=weights,
                       density=density, accountant=accountant)
