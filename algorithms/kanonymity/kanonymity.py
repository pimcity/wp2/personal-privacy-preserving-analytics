"""
This module contains a sort of interface for different k-anonymity algorithms.
"""


class Kanonymity:
    """
    Kanonymity class acts as a superclass for all the various algorithms that can be chosen to obtain the k-anonymity
    property. An inheritance over a pseudo-interface approach was preferred to ease the implementation of common
    functionalities.

    Attributes:
        k (int): k parameter for k-anonymity.
    """

    def __init__(self, k):
        self.k = k

    def perform(self):
        """
        This method needs to be extended to actually perform k-anonymization.

        Returns:
            None.
        """
        print("Starting k-anonymity algorithm")

    def check_kanon(self, df, columns_to_check, k):
        """
        This method checks if the input dataframe is k-anonymous having specified the k parameter and the dataframe
        columns over which perform the test.

        Args:
            df (:obj:`pandas.DataFrame`): input dataframe to be checked.
            columns_to_check (:obj:`list` of str): list of strings to consider when checking k-anonymity.
            k (int): k parameter for k-anonymity.

        Returns:
            bool: true if the input dataframe is k-anonymous considering input column/columns, false otherwise.
        """
        good = True
        grouped = df.groupby(columns_to_check)
        for key, item in grouped:
            a_group = grouped.get_group(key)
            if len(a_group) < k:
                good = False
                break
        return good
