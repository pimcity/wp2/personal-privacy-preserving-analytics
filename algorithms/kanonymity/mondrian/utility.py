"""This module contains the utility functions for a Mondrian algorithm implementation for k-anonymity. It's strongly
inspired but revisited work of the linked repository: https://github.com/qiyuangong/Mondrian.git. Please take
in mind that the documentation for this module can be found in large part consulting the original repo.
"""

from datetime import datetime
import time


def cmp(x, y):
    """
    Please refer to the documentation on the original repo.

    Args:
        x (int).
        y (int).

    Returns:
        int
    """
    if x > y:
        return 1
    elif x == y:
        return 0
    else:
        return -1


def cmp_str(element1, element2):
    """
    Please refer to the documentation on the original repo.

    Args:
        element1 (str).
        element2 (str).

    Returns:
        int
    """
    try:
        return cmp(int(element1), int(element2))
    except ValueError:
        return cmp(element1, element2)


def cmp_value(element1, element2):
    """
    Please refer to the documentation on the original repo.

    Args:
        element1 (Union[int, str]).
        element2 (Union[int, str]).

    Returns:
        int
    """
    if isinstance(element1, str):
        return cmp_str(element1, element2)
    else:
        return cmp(element1, element2)


def value(x):
    """
    Please refer to the documentation on the original repo.

    Args:
        x (Union[int, float]).

    Returns:
        (Union[int, float]).
    """
    if isinstance(x, (int, float)):
        return float(x)
    elif isinstance(x, datetime):
        return time.mktime(x.timetuple())
    else:
        try:
            return float(x)
        except Exception as e:
            return x


def merge_qi_value(x_left, x_right, connect_str='~'):
    """
    Please refer to the documentation on the original repo.

    Args:
        x_left (Union[int, float, str]).
        x_right (Union[int, float, str]).
        connect_str (char).

    Returns:
        str
    """
    result = ""
    if isinstance(x_left, (int, float)):
        if x_left == x_right:
            result = '%f' % (x_left)
        else:
            result = '%f%s%f' % (x_left, connect_str, x_right)
    elif isinstance(x_left, str):
        if x_left == x_right:
            result = x_left
        else:
            result = x_left + connect_str + x_right
    elif isinstance(x_left, datetime):
        # Generalize the datetime type value
        begin_date = x_left.strftime("%Y-%m-%d %H:%M:%S")
        end_date = x_right.strftime("%Y-%m-%d %H:%M:%S")
        result = begin_date + connect_str + end_date
    return result
