"""This module contains the internal components of a Mondrian algorithm implementation for k-anonymity. It's strongly
inspired but revisited work of the linked repository: https://github.com/qiyuangong/Mondrian.git. Please
take in mind that the documentation for this module can be found in large part consulting the original repo.
"""


class Partition(object):
    """
    Class for Group (or EC), which is used to keep records.

    Attributes:
        data (:obj:`list` of float): List of floats, for each dataframe row, corresponding to non categorical
            attributes.
        member (:obj:`list` of :obj:`list` of float): Records in group.
        low (:obj:`list` of float): Lower point, use index to avoid negative values.
        high (:obj:`list` of float): Higher point, use index to avoid negative values.
        allow (:obj:`list` of int): Show if partition can be split on this QI.
    """

    def __init__(self, data, low, high, qi_len):
        """
        Please refer to the documentation on the original repo.
        """
        self.low = list(low)
        self.high = list(high)
        self.member = data[:]
        self.allow = [1] * qi_len

    def add_record(self, record, dim):
        """
        Please refer to the documentation on the original repo.

        Args:
            record (:obj:`list` of float).
            dim (int).

        Returns:
            None: Appends to the class attribute 'self.member'.
        """
        self.member.append(record)

    def __len__(self):
        """
        Please refer to the documentation on the original repo.

        Returns:
            int.
        """
        return len(self.member)
