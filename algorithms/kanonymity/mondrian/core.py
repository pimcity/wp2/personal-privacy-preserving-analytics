"""This module contains the internal components of a Mondrian algorithm implementation for k-anonymity. It's strongly
inspired but revisited work of the linked repository: https://github.com/qiyuangong/Mondrian.git. Please
take in mind that the documentation for this module can be found in large part consulting the original repo.
"""

from functools import cmp_to_key
from algorithms.kanonymity.mondrian.partition import Partition
from algorithms.kanonymity.mondrian.utility import cmp_value, value, merge_qi_value
import pdb


class Core:
    """
    Attributes:
        data (:obj:`list` of float): List of floats, for each dataframe row, corresponding to QIs and whitelist
            attributes.
        qi_indexes (:obj:`list` of int): list of indexes of columns chosen as quasi-identifiers. This parameter is meant
            to be used by administrators. If no list is specified, the algorithm will consider all columns as
            quasi-identifiers.
        qi_len (int): Number of columns.
        k (int): K parameter for k-anonymity.
        result (:obj:`list` of :obj:`Partition`): List of Partition objects.
        qi_dict (:obj:`list` of :obj:`dict` of (:obj:`Union` [int, float], int)): List of dict, one dict for each column. For each
            column attribute domain, there's a dictionary with a float (if the attribute was non-categorical) or an
            integer (if the attribute was mapped from categorical to integer) as key and an integer as value: this
            represents the mapping between the attribute value ( the original one or the transformed one because it was
            categorical) and the index of the span for that attribute domain.
        qi_range (:obj:`list` of float): List of floats ranges, one for column attribute domain.
        qi_order (:obj:`list` of :obj:`list` of float): For each column, there's a list of attribute values ordered
            in ascendant order.
    """
    def __init__(self, data, qi_indexes, k):
        self.data = data
        self.qi_len = len(qi_indexes)
        self.qi_indexes = qi_indexes
        self.k = k
        self.result = []
        self.qi_dict = []
        self.qi_range = []
        self.qi_order = []

        att_values = []
        for i in range(self.qi_len):
            att_values.append(set())
            self.qi_dict.append(dict())
        for record in self.data:
            for i in range(self.qi_len):
                index = self.qi_indexes[i]
                att_values[i].add(record[index])
        for i in range(self.qi_len):
            value_list = list(att_values[i])
            value_list.sort(key=cmp_to_key(cmp_value))
            self.qi_range.append(value(value_list[-1]) - value(value_list[0]))
            self.qi_order.append(list(value_list))
            for index, qi_value in enumerate(value_list):
                self.qi_dict[i][qi_value] = index

    def mondrian_algo(self):
        """
        Please refer to the documentation on the original repo.

        Returns:
            :obj:`tuple` (param1, param2):

                param1 = :obj:`list` of :obj:`list` of str

                param2 = float
        """

        result = []
        data_size = len(self.data)
        low = [0] * self.qi_len
        high = [(len(t) - 1) for t in self.qi_order]
        whole_partition = Partition(self.data, low, high, self.qi_len)
        self._anonymize(whole_partition)
        ncp = 0.0
        for partition in self.result:
            rncp = 0.0
            for index in range(self.qi_len):
                rncp += self._get_normalized_width(partition, index)
            rncp *= len(partition)
            ncp += rncp
            for record in partition.member[:]:
                for index in range(self.qi_len):
                    record[self.qi_indexes[index]] = merge_qi_value(self.qi_order[index][partition.low[index]],self.qi_order[index][partition.high[index]])
                result.append(record)
        ncp /= self.qi_len
        ncp /= data_size
        ncp *= 100
        return (result), ncp

    def _anonymize(self, partition):
        """
        Please refer to the documentation on the original repo.

        Args:
            partition (:obj:`Partition`)

        Returns:
            None: appends to the class attribute 'self.result'.
        """

        allow_count = sum(partition.allow)
        # only run allow_count times
        if allow_count == 0:
            self.result.append(partition)
            return
        for index in range(allow_count):
            # choose attrubite from domain
            dim = self._choose_dimension(partition)
            if dim == -1:
                print("Error: dim=-1")
                pdb.set_trace()
            (split_val, next_val, low, high) = self._find_median(partition, dim)
            # Update parent low and high
            if low != '':
                partition.low[dim] = self.qi_dict[dim][low]
                partition.high[dim] = self.qi_dict[dim][high]
            if split_val == '' or split_val == next_val:
                # cannot split
                partition.allow[dim] = 0
                continue
            # split the group from median
            mean = self.qi_dict[dim][split_val]
            lhs_high = partition.high[:]
            rhs_low = partition.low[:]
            lhs_high[dim] = mean
            rhs_low[dim] = self.qi_dict[dim][next_val]
            lhs = Partition([], partition.low, lhs_high, self.qi_len)
            rhs = Partition([], rhs_low, partition.high, self.qi_len)
            for record in partition.member:
                pos = self.qi_dict[dim][record[self.qi_indexes[dim]]]
                if pos <= mean:
                    # lhs = [low, mean]
                    lhs.add_record(record, dim)
                else:
                    # rhs = (mean, high]
                    rhs.add_record(record, dim)
            # check is lhs and rhs satisfy k-anonymity
            if len(lhs) < self.k or len(rhs) < self.k:
                partition.allow[dim] = 0
                continue
            # anonymize sub-partition
            self._anonymize(lhs)
            self._anonymize(rhs)
            return
        self.result.append(partition)

    def _choose_dimension(self, partition):
        """
        Please refer to the documentation on the original repo.

        Args:
            partition (:obj:`Partition`).

        Returns:
            int
        """
        max_width = -1
        max_dim = -1
        for dim in range(self.qi_len):
            if partition.allow[dim] == 0:
                continue
            norm_width = self._get_normalized_width(partition, dim)
            if norm_width > max_width:
                max_width = norm_width
                max_dim = dim
        if max_width > 1:
            pdb.set_trace()
        return max_dim

    def _find_median(self, partition, dim):
        """
        Please refer to the documentation on the original repo.

        Args:
            partition (:obj:`Partition`).
            dim (int).

        Returns:
            :obj:`tuple` (param1, param2, param3, param4):

                param1 = :obj:`Union` [int, float]

                param2 = :obj:`Union` [int, float]

                param3 = :obj:`Union` [int, float]

                param4 = :obj:`Union` [int, float]
        """
        # use frequency set to get median
        frequency = self._frequency_set(partition, dim)
        split_val = ''
        next_val = ''
        value_list = list(frequency.keys())
        value_list.sort(key=cmp_to_key(cmp_value))
        total = sum(frequency.values())
        middle = total // 2
        if middle < self.k or len(value_list) <= 1:
            try:
                return '', '', value_list[0], value_list[-1]
            except IndexError:
                return '', '', '', ''
        index = 0
        split_index = 0
        for i, qi_value in enumerate(value_list):
            index += frequency[qi_value]
            if index >= middle:
                split_val = qi_value
                split_index = i
                break
        else:
            print("Error: cannot find split_val")
        try:
            next_val = value_list[split_index + 1]
        except IndexError:
            # there is a frequency value in partition
            # which can be handle by mid_set
            # e.g.[1, 2, 3, 4, 4, 4, 4]
            next_val = split_val
        return (split_val, next_val, value_list[0], value_list[-1])

    def _get_normalized_width(self, partition, index):
        """
        Please refer to the documentation on the original repo.

        Args:
            partition (:obj:`Partition`).
            index (int).

        Returns:
            :obj:`Union` [int, float].
        """
        d_order = self.qi_order[index]
        width = value(d_order[partition.high[index]]) - value(d_order[partition.low[index]])
        if width == self.qi_range[index]:
            return 1
        return width * 1.0 / self.qi_range[index]

    def _frequency_set(self, partition, dim):
        """
        Please refer to the documentation on the original repo.

        Args:
            partition (:obj:`Partition`).
            dim (int).

        Returns:
            :obj:`Union` [int, float, :obj:`dict`].
        Raises:
            KeyError.
        """
        frequency = {}
        for record in partition.member:
            try:
                frequency[record[self.qi_indexes[dim]]] += 1
            except KeyError:
                frequency[record[self.qi_indexes[dim]]] = 1
        return frequency