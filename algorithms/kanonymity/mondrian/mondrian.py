"""This module contains the external components of a Mondrian algorithm implementation for k-anonymity. It's is
strongly inspired but revisited work of the linked repository: https://github.com/qiyuangong/Mondrian.git. Please take
in mind that the core elements were kept more or less the same. The untouched code fragments will be marked, the
corresponding documentation can be found on the original repo.
We propose a modified version of it:
- Except for some outliers, this project should anonymize all datasets while the original work only permits as input two
specific scientific datasets whose parameters are hard-coded.
- It is possible to choose column attributes over which the k-anonymity property is checked before performing the Mondrian
algorithm.
- By default, all attributes are considered as QIAs but to administrators it is given the possibility to select the desired
set of QIAs.
- A "whitelist" function is provided; administrators can select one or more columns and the algorithm will not consider
them for checking the k-anonymity property but it will display them anyway in the final anonymized dataset. This functionality
can be exploited for classification purposes, selecting the sensitive column as the class label values.
"""

from algorithms.kanonymity.kanonymity import Kanonymity
import numpy as np
import pandas as pd
from itertools import combinations
from algorithms.kanonymity.mondrian.core import Core


class Mondrian(Kanonymity):
    """
    This class extends the Kanonymity class.

    Attributes:
        k (int): k parameter for k-anonymity.
        user_choice_index (:obj:`list` of int): list of integers. Represents the indexes of the dataframe columns over
            which the k-anonymity property is checked before performing the Mondrian algorithm. If the user doesn't
            express any preferences about columns to check before manipulating data, the user_choice_index attribute is
            None.
        attr_names (:obj:`list` of str): list of strings of all dataframe column attributes.
        qi_indexes (:obj:`list` of int): list of indexes of columns chosen as quasi-identifiers. This parameter is meant
            to be used by administrators. If no list is specified, the algorithm will consider all columns as
            quasi-identifiers.
        whitelist (:obj:`list` of int): list of indexes of columns chosen to be not considered for k-anonymity process.
            These columns will be just displayed as they are.
        qi_indexes_final (:obj:`list` of int): same as 'qi_indexes' variable, but with adjusted (translated) indexes
            after removing columns that are neither quasi-identifiers nor whitelisted.
        whitelist_final (:obj:`list` of int): same as 'whitelist' variable, but with adjusted (translated) indexes after
            removing columns that are neither quasi-identifiers or whitelisted.
        GCP (float): Global Certainty Penalty. For further references please refer to the paper: 'J. XU, W. WANG,
            J. P El, X. WANG, B. SHI, and A. FU, "Utility-based anonymization using local recoding," in Proc. of the
            12th ACM SIGKDD. Philadelphia, PA, 2006'.
    """

    def __init__(self, k, user_choice_index=None, qi_indexes=None, whitelist=None):
        Kanonymity.__init__(self, k)

        self.attr_names = None
        self.user_choice_index = user_choice_index
        self.qi_indexes = qi_indexes
        self.whitelist = whitelist
        self.qi_indexes_final = None
        self.whitelist_final = None
        self.GCP = None

    def perform(self, df):
        """
        Extends the perform method of Kanonymity class. It's the only method that has to be called from outside of this
        module, all the operations are made here. If the user has specified the indexes of columns he is particularly
        interested in, the _check_kanon_from_columns method tries to find any combinations of column attributes for
        which the resulting dataframe is already k-anonymous. If this is not the case, the Mondrian algorithm is
        performed.

        Args:
            df (:obj:`pandas.DataFrame`): input dataframe to be checked.

        Returns:
            :obj:`pandas.DataFrame`: the final dataframe: can be composed by one or more non modified columns if the
            combinations of the column attributes assemble already a k-anonymized dataframe; or can be a manipulated
            version of the original dataframe, considering all column attributes as quasi-identifiers.
        """
        super(Mondrian, self).perform()
        print('Choosen algorithm : Mondrian')

        if self.user_choice_index:
            kanon_columns = self._check_kanon_from_columns(df, self.user_choice_index)
            if kanon_columns:
                col_names = np.array(list(df.columns))[list(kanon_columns)].tolist()
                print("Mondrian finished")
                return df[col_names]
            else:
                print("Sorry, no one of the columns ( or their combinations ) you selected is k_anonymous. Every "
                      "column will be taken into account to obtain k_anonymity (if 'qi_indexes' has not been selected)")
        data, intuitive_order = self._read_data(df)
        df = self._get_result(data, intuitive_order)
        print("Mondrian finished")
        return df

    def gcp(self):
        """
        This method has to be called after the Mondrian algorithm, in fact, the NCP metric is calculated using the
        partitions list, among other things.

        Returns:
            Float.
        """

        if self.GCP is None:
            print("Please, run Mondrian algorithm before: it means that if you're getting back columns without "
                  "manipulation obviously NCP can't be performed")
            return
        else:
            print("NCP = %.2f %%" % self.GCP)
            return self.GCP

    def _check_kanon_from_columns(self, df, user_choice_index):
        """
        This method receives a dataframe and indexes of columns. It will calculate the simple combinations set of
        columns and rank it according to first to sub-set length and then to columns priority order. Finally it will
        return the top set, if any, according to the ordering.

        Args:
            df (:obj:`pandas.DataFrame`): input dataframe to be checked.
            user_choice_index (:obj:`list` of int): list of integers. Represents the indexes of the dataframe columns
                over which the k-anonymity property is checked before performing the Mondrian algorithm.

        Returns:
            :obj:`Union` [:obj:`tuple` of int, None]: If any, the returned tuple, containing one or more columns, that
            represents the final combination found; or None if not combination is suitable.
        """

        working_set = []
        for i in range(1, len(user_choice_index) + 1):
            for el in combinations(user_choice_index, i):
                working_set.append(el)
        working_set = [[working_set[i], i] for i, el in enumerate(working_set)]
        final_set = []
        while working_set:
            tmp_work_est = working_set.copy()
            for work_tuple in tmp_work_est:
                # if a columns is not k-anonymous, all subsets containing that columns will be removed
                cols = np.array(list(df.columns))[list(work_tuple[0])].tolist()
                if not self.check_kanon(df, cols, self.k):
                    tmp_work = working_set.copy()
                    for x in tmp_work:
                        if all(elem in x[0] for elem in work_tuple[0]):
                            working_set.remove(x)
                else:
                    final_set.append(work_tuple)
                    working_set.remove(work_tuple)
                tmp_work_est = working_set.copy()
        final_comb = []
        if final_set:
            max_len = max(final_set, key=lambda x: len(x[0]))
            num_same_len = [el for el in final_set if len(el[0]) == len(max_len[0])]
            final_comb = min(num_same_len, key=lambda x: x[1])
        if final_comb:
            return final_comb[0]
        else:
            return None

    def _read_data(self, df):
        """
        This method takes as input a dataframe and creates the data structures that will be used to execute the Mondrian
        algorithm. The column attributes that are taken as quasi-identifier and considered when performing the algorithm
        to obtain k-anonymity, can be modified throw Mondrian class parameter "qi_indexes" during the class creation.
        The standard behavior is to consider all columns as quasi-identifiers. If the "whitelist" parameter is present,
        columns indexes are adjusted; "qi_indexes_final" and "whitelist_final" are set after removing columns that are
        neither QIs nor whitelisted: this parameters will be used to work with QIs columns and maintain at the same time
        untouched the whitelist columns.

        Args:
            df (:obj:`pandas.DataFrame`): input dataframe to transform into internal data structures.

        Returns:
            :obj:`tuple` (param1, param2):
                param1 = :obj:`list` of float: list of floats, for each dataframe row, corresponding to non categorical
                attributes.

                param2 = :obj:`list` of :obj:`list` of str: list of lists of strings corresponding, for each
                column, to its attribute domain.
        """

        self.attr_names = [column for column in df.columns]
        if not self.qi_indexes:
            self.qi_indexes = [i for i in range(len(self.attr_names))]
            if not self.whitelist:
                self.qi_indexes_final = [i for i in range(len(self.qi_indexes))]
                self.whitelist_final = []
        elif self.qi_indexes:
            if not self.whitelist:
                self.qi_indexes_final = [i for i in range(len(self.qi_indexes))]
                self.whitelist_final = []
        if self.whitelist:
            self.qi_indexes = [i for i in self.qi_indexes if i not in self.whitelist]
            min_l = min(self.qi_indexes + self.whitelist)
            max_l = max(self.qi_indexes + self.whitelist)
            tot = [i for i in range(min_l, max_l + 1)]
            self.qi_indexes_final = []
            self.whitelist_final = []
            count = 0
            for el in tot:
                if el in self.qi_indexes:
                    self.qi_indexes_final.append(count)
                    count += 1
                elif el in self.whitelist:
                    self.whitelist_final.append(count)
                    count += 1



        is_cat = [False if df.dtypes[el] == "int64" or df.dtypes[el] == "float64" else True for el in self.qi_indexes]
        data = []
        # oder categorical attributes in intuitive order
        # here, we use the appear number
        intuitive_dict = []
        intuitive_order = []
        intuitive_number = []
        for i in range(len(self.qi_indexes)):
            intuitive_dict.append(dict())
            intuitive_number.append(0)
            intuitive_order.append(list())

        for index, row in df.iterrows():
            temp = []
            for el in row:
                temp.append(el)
            ltemp = []
            for i in range(len(self.qi_indexes)):
                index = self.qi_indexes[i]
                if is_cat[i]:
                    try:
                        ltemp.append(intuitive_dict[i][temp[index]])
                    except KeyError:
                        intuitive_dict[i][temp[index]] = intuitive_number[i]
                        ltemp.append(intuitive_number[i])
                        intuitive_number[i] += 1
                        intuitive_order[i].append(temp[index])
                else:
                    ltemp.append(float(temp[index]))
            if self.whitelist:
                for j in range(len(self.whitelist)):
                    ltemp.insert(self.whitelist_final[j], temp[self.whitelist[j]])
            data.append(ltemp)
        return data, intuitive_order

    def _convert_to_df(self, result):
        """
        This method simply converts data from the internal list of lists format to pandas.Dataframe format, taking out
        columns that are neither QIs nor whitelisted from the list comprehending all columns names "attr_names".

        Args:
            result (:obj:`list` of :obj:`list` of str): this is the result of the Mondrian algorithm in the internal
                list of lists of strings format, one list of strings for each dataframe row.

        Returns:
            :obj:`pandas.DataFrame`: the output dataframe.
        """

        columns = [self.attr_names[i] for i in range(len(self.attr_names)) if
                   i in self.qi_indexes or (self.whitelist and i in self.whitelist)]
        df = pd.DataFrame(result, columns=columns)
        return df

    def _get_result(self, data, intuitive_order):
        """
        This method creates the Core object, obtains the list of partitions from the Mondrian algorithm, and returns the
        final dataframe after conversion into a pandas.Dataframe.

        Args:
            data(:obj:`list` of float): list of floats, for each dataframe row, corresponding to non categorical
                attributes.
            intuitive_order (:obj:`list` of :obj:`list` of str): list of lists of strings corresponding, for each
                column, to its attribute domain.

        Returns:
            :obj:`pandas.DataFrame`: the output dataframe.
        """
        core = Core(data, self.qi_indexes_final, self.k)
        partition_list, self.GCP = core.mondrian_algo()
        # Convert numerical values back to categorical values if necessary
        result = self._convert_to_raw(partition_list, intuitive_order)
        df = self._convert_to_df(result)
        return df

    def _convert_to_raw(self, result, intuitive_order):
        """
        During preprocessing, categorical attributes are convert to numeric attribute using intuitive order. This
        function will convert these values back to they raw values. For example, Female and Male may be converted to 0
        and 1 during anonymization. Then we need to transform them back to original values after anonymization.

        Args:
            result (:obj:`list` of :obj:`list` of str): list of lists of strings; if a string (but actually an index)
                corresponds to a categorical attribute, then it will be reconverted to a real string (the previous
                attribute value) thanks to intuitive_order variable.
            intuitive_order (:obj:`list` of :obj:`list` of str): list of lists of strings corresponding, for each
                column, to its attribute domain.

        Returns:
            :obj:`list` of :obj:`list` of str: this is the result of the Mondrian algorithm in the internal
            list of lists of strings format, one list of strings for each dataframe row.
        """

        connect_str = '~'
        covert_result = []
        qi_len = len(intuitive_order)
        for record in result:
            covert_record = []
            for i in range(qi_len):
                if len(intuitive_order[i]) > 0:
                    vtemp = ''
                    if connect_str in record[self.qi_indexes_final[i]]:

                        temp = record[self.qi_indexes_final[i]].split(connect_str)
                        raw_list = []
                        for j in range(int(float(temp[0])), int(float(temp[1])) + 1):
                            raw_list.append(str(intuitive_order[i][j]))
                        vtemp = connect_str.join(raw_list)
                    else:
                        vtemp = intuitive_order[i][int(float(record[self.qi_indexes_final[i]]))]
                    covert_record.append(vtemp)
                else:
                    covert_record.append(record[self.qi_indexes_final[i]])
            if self.whitelist_final:
                for j in range(len(self.whitelist_final)):
                    covert_record.insert(self.whitelist_final[j], record[self.whitelist_final[j]])
            covert_result.append(covert_record)
        return covert_result
