import zanonymity.zanon
import pandas as pd

"""
This module contains a wrapper for the z-anonymity.
"""

class zanon:
    """
    Z-anonymity: anonymization for data streams

    Attributes:
        deltat (int): parameter that regulates the memory of the system.
        zeta (int): minimum number of users exposing an attribute to make it be released.
    """

    def __init__(self, deltat, zeta):
        self.z = zanonymity.zanon.zanon(deltat, 0.0, 1)
        self.zeta = zeta

    def perform(self, df, time_col, user_col, attribute_col):
        """
        Run z-anonymity on a dataframe.

        Args:
            df (:obj:`pandas.DataFrame`): input dataframe to be checked.
            time_col (str): DataFrame column for the time.
            user_col (str): DataFrame column for the user identifier.
            attribute_col (str): DataFrame column for the attribute/item.
        """

        for row in df.itertuples():
            t = getattr(row, time_col)
            u = getattr(row, user_col)
            a = getattr(row, attribute_col)
            self.z.anonymize((t,u,a))

        df = pd.DataFrame(self.z.out_tuple, columns = [time_col, user_col, attribute_col, "z"])
        df["z"] = df["z"].apply(lambda e: e[0])
        df[attribute_col].loc[df['z'] < self.zeta] = None
        df["z"].loc[df['z'] < self.zeta] = None

        return df
