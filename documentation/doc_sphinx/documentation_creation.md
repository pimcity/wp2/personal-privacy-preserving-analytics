### Documentation creation

In the ***doc_sphinx*** folder are located the necessary files to create an HTML documentation.

This guide assumes you have installed the ***sphinx-rtd-theme***, just to look nicer. Please refer to [sphinx-rtd-theme · PyPI](https://pypi.org/project/sphinx-rtd-theme/) .

Windows example:

1. Open a terminal window and navigate to the same directory of the *make.bat* file.

   > *cd C:\path_to_the_project\pppa\documentation\doc_sphinx*

2. Execute make command.

   > make html

3. Navigate to *_build\html* folder and open the ***index.html*** file.



If you want to modify code path or other settings you must edit the *conf.py* file. In general for sphinx setup, please refer to [Overview — Sphinx 4.0.0+ documentation (sphinx-doc.org)](https://www.sphinx-doc.org/en/master/#).