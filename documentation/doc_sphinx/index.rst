.. pppa documentation master file, created by
   sphinx-quickstart on Sun Dec  6 13:23:28 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. toctree::
   :maxdepth: 2
   :caption: Contents:


Intro
******************

.. include:: intro.txt

Project Structure
******************

.. include:: directory_structure.txt
    :literal:

Requirements
******************
.. include:: requirements.txt
    :literal:

Usage Examples
******************
.. include:: example_code.txt
License
******************
Code Documentation
******************
Algorithms
==================
K-anonymity
------------------
* ``kanonymity`` module

 .. automodule:: kanonymity
     :members:
     :private-members:

* ``mondrian`` module

 .. automodule:: mondrian
     :members:
     :private-members:

* ``core`` module

 .. automodule:: core
     :members:
     :private-members:

* ``partition`` module

 .. automodule:: partition
     :members:
     :private-members:

* ``utility`` module

 .. automodule:: utility
     :members:
     :private-members:

Z-anonymity
------------
* ``zanon`` module

 .. automodule:: zanon
     :members:
     :private-members:

Differential privacy
---------------------
* ``differential_privacy`` module

 .. automodule:: differential_privacy
     :members:
     :private-members:

* ``dp_IBM_wrapper`` module

 .. automodule:: dp_IBM_wrapper
     :members:
     :private-members:

* ``dp_functions`` module

 .. automodule:: dp_functions
     :members:
     :private-members:

Data_manage
==================
MongoBD
-----------

* ``mongoDB_setup`` module

 .. automodule:: mongoDB_setup
     :members:
     :private-members:

PostgreSQL
-----------

* ``postgreSQL_manage`` module

 .. automodule:: postgreSQL_manage
     :members:
     :private-members:

CSV
-----------

* ``CSV_manage`` module

 .. automodule:: CSV_manage
     :members:
     :private-members:

Dataframe_utility
------------------

* ``Dataframe_utility`` module

 .. automodule:: dataframe_utility
     :members:
     :private-members:


Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
